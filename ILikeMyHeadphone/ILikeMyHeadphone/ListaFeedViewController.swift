//
//  ListaFeedViewController.swift
//  ILikeMyHeadphone
//
//  Created by Carlos Henrique on 25/07/15.
//  Copyright (c) 2015 Unifor. All rights reserved.
//

import UIKit

class ListaFeedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var reviews: [Review] = [Review]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        populateReviews()
        
        /* Alterando as cores da barra de navegação */
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        /* Inclusão do botão de novo feed */
        let addFeed = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Compose, target: self, action: "exibirTelaNovoReview")
        self.navigationItem.rightBarButtonItem = addFeed
        
        /* TODO: Controlar o login corretamente */
        self.performSegueWithIdentifier("exibirTelaLogin", sender: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviews.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: FeedlTableViewCell = tableView.dequeueReusableCellWithIdentifier("feedCell", forIndexPath: indexPath) as! FeedlTableViewCell
        
        var review = reviews[indexPath.row]
        cell.lbComment.text = review.comment
        cell.lbBrand.text = review.brand
        cell.lbModel.text = review.model
        cell.imgRating.image = UIImage(named: "rating-\(review.rating!)-stars")
        
        return cell
    }
    
    func populateReviews() {
        var review = Review()
        
        review.brand = "Sony"
        review.model = "AX2892"
        review.rating = 3
        review.comment = "Ótimo fone de ouvido. Gostei bastante."
        review.photo = "http://"
        reviews.append(review)
        
        review = Review()
        review.brand = "Beats"
        review.model = "SOLA 3D"
        review.rating = 4
        review.comment = "Design bem bonito e ótima qualidade, mas bem caro."
        review.photo = "http://"
        reviews.append(review)
        
        review = Review()
        review.brand = "Panasonic"
        review.model = "AX4938"
        review.rating = 2
        review.comment = "Bom custo benefício."
        review.photo = "http://"
        reviews.append(review)
        
        review = Review()
        review.brand = "Razer"
        review.model = "Tiamat"
        review.rating = 5
        review.comment = "Muito caro, porém de excelente qualidade."
        review.photo = "http://"
        reviews.append(review)
        
        review = Review()
        review.brand = "Multilaser"
        review.model = "MT0342"
        review.rating = 1
        review.comment = "Preço bem acessível, porém com um acabamento bem ruim."
        review.photo = "http://"
        reviews.append(review)
        
        review = Review()
        review.brand = "Sennheiser"
        review.model = "PC 360"
        review.rating = 4
        review.comment = "Um dos melhores fones de ouvido que usei. Ótimo para jogos"
        review.photo = "http://"
        reviews.append(review)
        
        review = Review()
        review.brand = "Samsung"
        review.model = "Level Over"
        review.rating = 3
        review.comment = "Ótima qualidade, porém muito caro e muito pesado."
        review.photo = "http://"
        reviews.append(review)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
