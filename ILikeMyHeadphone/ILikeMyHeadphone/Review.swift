//
//  Review.swift
//  ILikeMyHeadphone
//
//  Created by Carlos Henrique on 28/07/15.
//  Copyright (c) 2015 Unifor. All rights reserved.
//

import UIKit

class Review: NSObject {

    var id: Int?
    var user: Int?
    var brand: String?
    var model: String?
    var rating: Int?
    var comment: String?
    var photo: String?
    
}
