//
//  FeedlTableViewCell.swift
//  ILikeMyHeadphone
//
//  Created by Carlos Henrique on 28/07/15.
//  Copyright (c) 2015 Unifor. All rights reserved.
//

import UIKit

class FeedlTableViewCell: UITableViewCell {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lbComment: UILabel!
    @IBOutlet weak var imgRating: UIImageView!
    @IBOutlet weak var lbBrand: UILabel!
    @IBOutlet weak var lbModel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
