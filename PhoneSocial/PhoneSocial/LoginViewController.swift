//
//  LoginViewController.swift
//  PhoneSocial
//
//  Created by Carlos Henrique on 08/07/15.
//  Copyright (c) 2015 Unifor. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfSenha: UITextField!
    
    var loginManager: LoginManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loginManager = LoginManager()
        
        /* Alterando cores da barra de navegação */
//        self.navigationController?.navigationBar.barTintColor = UIColor(red: 61.0/255.0, green: 88.0/255.0, blue: 148.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        /* Alterando os ícones de notificação */
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func efetuarLogin(sender: UIButton) {
        /* Validação de email */
        var email = tfEmail.text
        if !validarEmail(email) {
            exibirMensagemErro("Informe um email válido")
            return
        }
        
        /* Validação de senha */
        var tamanhoSenha = tfSenha.text
        if tamanhoSenha == nil ||
            count(tamanhoSenha.stringByTrimmingCharactersInSet(
                NSCharacterSet.whitespaceCharacterSet())) < 6 {
            exibirMensagemErro("A senha deve conter pelo menos 6 caracteres")
                    return
                
        }
        
        if (loginManager.verificarExistenciaUsuario(tfEmail.text, senha: tfSenha.text)) {
            /* Exibir a tela de feeds */
            self.dismissViewControllerAnimated(true, completion: nil)
        } else {
            exibirMensagemErro("Login/senha inválida")
        }
        
    }
    
    func validarEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    func exibirMensagemErro(mensagem: String) {
        var aviso = UIAlertView()
        aviso.addButtonWithTitle("OK")
        aviso.title = "Atenção"
        aviso.message = mensagem
        
        aviso.show()
    }
    
    override func viewWillAppear(animated: Bool) {
        /* Escondendo a barra de navegação */
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        /* Definindo listeners para o teclado */
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func keyboardWillShow(notification: NSNotification) {
        self.view.frame.origin.y = -100.0
    }
    
    func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0.0
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
