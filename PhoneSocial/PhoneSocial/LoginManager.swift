//
//  LoginManager.swift
//  PhoneSocial
//
//  Created by Carlos Henrique on 12/07/15.
//  Copyright (c) 2015 Unifor. All rights reserved.
//

import UIKit

class LoginManager: NSObject {
    
    let usuarios = ["carlosce@gmail.com": "123456",
                    "monique.onofre@gmail.com": "789012"]
    
    func verificarExistenciaUsuario(email: String, senha: String) -> Bool {
        var senhaRegistrada = usuarios[email]
        
        if senha == senhaRegistrada {
            return true
        } else {
            return false
        }
    }
    
}
